<!DOCTYPE html>
<?php
include 'connection.php';
include 'aksi_data_mhs.php';
?>
<html>
    <head>
        <title>Sistem Informasi Akademik</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

    </head>
    <body>
        <a href="input_data_mhs.php" class="btn btn-primary">Tambah Data Mahasiswa</a>
        <table class="table table-bordered table-striped" >
            <thead>
                <tr>
                    <th class="text-center" width="5%">nomor</th>
                    <th class="text-center">nama</th>
                    <th class="text-center">alamat</th>
                    <th class="text-center">no hp</th>
                    <th colspan="2" class="text-center" width="15%">aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $query_select = "select * from mahasiswa";
                $data = mysqli_query($dbs, $query_select);
                //var_dump(mysqli_fetch_assoc($data));
                // menampilkan data yang diinput ke database ke dalam bentuk tabel
                $i = 1;
                while ($row = mysqli_fetch_assoc($data)) {
                    echo "<tr>
                    <td class='text-center'>$i</td>
                    <td>$row[mhs_nama]</td>
                    <td>$row[mhs_alamat]</td>
                    <td>$row[mhs_tlp]</td>
                    <td class=\"text-center\"><a href='ubah_data_mhs.php?mhs_id=$row[mhs_id]' class='fa fa-pencil-square-o'></a></td>
                    <td class='text-center'><a href='?aksi=hapus&mhs_id=$row[mhs_id]' class='fa fa-trash'></a></td>    
                    </tr>";
                    $i++;
                }
                ?>
            </tbody>
        </table>
    </body>
</html>
