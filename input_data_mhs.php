<!DOCTYPE html>
<?php
include "connection.php";
include "aksi_data_mhs.php";
?>
<html>
    <head>
        <title>Sistem Informasi Akademik</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        
    </head>
    <body>
        <div class="jumbotron text-center">
            <h1>Form Input Data Mahasiswa</h1>
            <!--<p>Resize this responsive page to see the effect!</p>--> 
        </div>
        <div class="col-sm-6 col-sm-offset-3">
            <form method="post" action="?aksi=tambah">
                <div class="form-group">
                    <label for="nama">Nama:</label>
                    <input name="nama" class="form-control" id="nama" required="">
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat:</label>
                    <textarea name="alamat" class="form-control" rows="3" id="alamat" required=""></textarea>
                </div>
                <div class="form-group">
                    <label for="no_hp">No handphone:</label>
                    <input name="no_hp" class="form-control" id="no_hp" required="">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-default">Submit</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                </div>
            </form>
        </div>
    </body>
</html>
