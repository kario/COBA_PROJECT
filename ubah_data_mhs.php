<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
include "connection.php";
include "aksi_data_mhs.php";
$query_select = "select * from mahasiswa where mhs_id = $_GET[mhs_id]";
$data = mysqli_query($dbs, $query_select);
while ($row = mysqli_fetch_assoc($data)) {
    $nama = $row['mhs_nama'];
    $almt = $row['mhs_alamat'];
    $tlpx = $row['mhs_tlp'];
}
?>
<html>
    <head>
        <title>Sistem Informasi Akademik</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="jumbotron text-center">
            <h1>Form Input Data Mahasiswa</h1>
        </div>
        <div class="col-sm-6 col-sm-offset-3">
            <form method="post" action="?aksi=ubah&mhs_id=<?php echo $_GET['mhs_id']; ?>">
                <div class="form-group">
                    <label for="nama">Nama:</label>
                    <input name="nama" value="<?php echo $nama; ?>" class="form-control" id="nama" maxlength="5" required="">
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat:</label>
                    <textarea name="alamat" class="form-control" rows="3" id="alamat"><?php echo $almt; ?></textarea>
                </div>
                <div class="form-group">
                    <label for="no_hp">No handphone:</label>
                    <input name="no_hp" value="<?php echo $tlpx; ?>" class="form-control" id="no_hp">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-default">Update</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                </div>
            </form>
        </div>
    </body>
</html>